# Voyage en Cybérie
## Le chiffrement, principes et applications

Durée 1 h 30 environ

Le chiffrement, souvent appelé à tort « cryptage », est une notion à travers laquelle nous passons de temps en temps, au fin fond des paramètres de nos appareils informatiques, ou à la lecture des recommandations de sécurité et autres actualités de cybersécurité. « Sans doute un truc qui ne concerne que les experts » penserez-vous peut-être. « Encore du charabia incompréhensible » ajouteront les plus désabusés.  

Pourtant, le chiffrement est un élément essentiel de la sécurité informatique, et nous sommes toutes et tous protégés par des protocoles de chiffrement sans même le savoir. De plus, de nombreux outils accessibles nous permettent d'utiliser le chiffrement pour notre bénéfice et sans être un expert en cryptographie.

Au cours de ce voyage en « Cybérie », nous donnerons une définition de ce qu'est le chiffrement, et nous verrons quelques cas d'utilisation du chiffrement au quotidien. Nous mettrons ensuite en place le chiffrement de bout en bout de nos courriels avec Thunderbird et OpenPGP.

Pré-requis :  
* Être très à l’aise avec un ordinateur, savoir le maintenir à jour, maîtriser la notion de fichier, la hiérarchie des dossiers sur le disque... 💻
* Disposer d’un ordinateur portable sous Windows, macOS ou Linux sur lequel vous pouvez installer des applications, et que vous pourrez amener le jour du voyage en Cybérie 😀
* [Pour une session présentielle] Savoir faire un partage de connexion entre votre téléphone et votre ordinateur 📱
* Avoir une adresse e-mail personnelle chez un fournisseur proposant les protocoles POP3/IMAP et SMTP afin d'utiliser un client mail tiers (la plupart des fournisseurs le proposent) ✉️
* Avoir envie d'essayer le chiffrement de bout en bout avec cette adresse e-mail (rassurez-vous, si cela ne vous plaît pas, ça n'a rien d'irréversible !) 🔐
