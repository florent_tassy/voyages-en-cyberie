Le voyage en Cybérie approche.  

Afin de pouvoir réaliser les activités le jour J, il est recommandé d'installer et de paramétrer une applications en avance. Cela vous évitera de devoir le faire le jour J en partage de connexion avec votre téléphone.  

* Téléchargez et installez Thunderbird pour votre ordinateur : [https://www.thunderbird.net/fr/](https://www.thunderbird.net/fr/) 

* Ajoutez votre compte courriel à Thunderbird
