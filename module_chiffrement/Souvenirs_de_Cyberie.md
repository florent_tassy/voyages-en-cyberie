Voici une petite mise en pratique qui devrait vous replonger dans les doux souvenirs de Cybérie…  

1. Si vous avez une clé publique dans votre Thunderbird pour mon adresse ftassy@e.email, effacez-là. Elle n'est qu'une clé créée pour les besoin de la session, et je l'effacerai à la fin de chaque session.
2. Envoyez-moi un courriel **en clair**, **signé**, avec votre **clé publique en pièce jointe**, pour me demander ma clé publique usuelle.
3. À la réception de votre message, je vais enregistrer votre clé publique et vous répondre avec ma clé publique en pièce jointe.
4. Lorsque vous recevrez ma réponse, enregistrez ma clé publique.
5. Envoyez-moi un nouveau message **chiffré**.
6. Je vous confirmerai la bonne réception de votre message chiffré et vous poserai une question.
7. Envoyez-moi la réponse à la question (rien d'insurmontable 😉) en précisant si vous souhaitez obtenir l’open badge correspondant à l’exercice, ainsi que le nom auquel vous souhaitez que je le délivre. Les commentaires, les avis, les suggestions ainsi que les questions sont également bienvenus.  

Si vous souhaitez en apprendre davantage sur les open badges, vous pouvez visiter [https://openbadges.info/](https://openbadges.info/). Si le principe vous plaît, n’hésitez pas à créer un compte sur « [Open Badge Passport](https://openbadgepassport.com/1accueil/) » (en Français) ou « [Badgr](https://eu.badgr.com/auth/login) » (English only) et à y importer vos badges de Cybérie 😉  