[![CC BY 4.0][cc-by-shield]][cc-by]
# Voyages en Cybérie

Les voyages en Cybérie sont une série de présentations autour de la cybersécurité et du logiciel libre.

Ils tentent d'allier un minimum de théorie et un maximum de mise en pratique.

Vient ensuite un exercice pratique reprenant les principes de la session à réaliser en autonomie.

Sujets :  
[Gestion sécurisée de mots de passe](./module_mots_de_passe/)  
[Le chiffrement, principes et applications](./module_chiffrement/)  


[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
